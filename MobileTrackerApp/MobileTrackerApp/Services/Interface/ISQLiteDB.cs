﻿using SQLite;

namespace FinalTracker.Interfaces
{
    public interface ISQLiteDB
    {
        SQLiteConnection GetConnection();
    }
}
