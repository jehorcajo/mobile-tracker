﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Services
{
    public class ApiWebService
    {
        public const string MainIPaddress = "192.168.137.1";
        public const string IPaddressPort = "5003";
        public const string MainWebServiceUrl = "http://192.168.137.1:5003/";
        public const string LoginWebServiceUrl = "http://testingme333-001-site1.etempurl.com/api/UserCredentials/";
    }
}
