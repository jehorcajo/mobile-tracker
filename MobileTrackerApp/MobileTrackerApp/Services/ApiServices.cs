﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MobileTrackerApp.Services
{
    public class ApiServices<T>
    {
        public const string MainIPaddress = "192.168.137.1";
        public const string IPaddressPort = "5003";
        public const string MainWebServiceUrl = "http://192.168.137.1:5003/";
        public const string LoginWebServiceUrl = "http://testingme333-001-site1.etempurl.com/api/UserCredentials/";

        public async Task<bool> checkLogin(string username, string password)
        {
            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(LoginWebServiceUrl + "username=" + username + "/" + "password=" + password);

            return response.IsSuccessStatusCode;
        }
    }
}
