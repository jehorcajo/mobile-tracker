﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace MobileTrackerApp.Services
{
    public class Common
    {
        public static byte[] GetRandomSalt(int length)
        {
            var Random = new RNGCryptoServiceProvider();
            byte[] salt = new byte[length];
            Random.GetNonZeroBytes(salt);
            return salt;
        }
        

        public static byte[] SaltHashPassword(byte[] password, byte[] salt)
        {
            HashAlgorithm algo = new SHA256Managed();
            byte[] plainTextWithSaltBytes = new byte[password.Length + salt.Length];
            for (int i = 0; i < password.Length; i++)
                plainTextWithSaltBytes[i] = password[i];
            for (int i = 0; i < salt.Length; i++)
                plainTextWithSaltBytes[password.Length + i] = salt[i];
            return algo.ComputeHash(plainTextWithSaltBytes);
        }

        public static Tuple<byte[], string> CryptoHash(string password)
        {
            var passhash = ASCIIEncoding.ASCII.GetBytes(password);
            byte[] datainput = new byte[passhash.Length];
            for (int i = 0; i < passhash.Length; i++)
            {
                datainput[i] = passhash[i];
            }
            SHA512 sha = new SHA512Managed();
            var hashbyte = sha.ComputeHash(datainput);
            string hashresult = Convert.ToBase64String(hashbyte);


            return new Tuple<byte[], string>(new byte[20], hashresult);
        }
    }
}
