﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using Entry = Microcharts.Entry;
using Xamarin.Forms.Xaml;
using Microcharts;
using SkiaSharp;
using MobileTrackerApp.Models;

namespace MobileTrackerApp.ViewModels
{
    public class UserActivityViewModel : BaseViewModel
    {
        public string empid { get; set; }
        public string prevsatdate { get; set; }
        public string prevsundate { get; set; }
        public string prevmondate { get; set; }
        public string prevtuedate { get; set; }
        public string prevweddate { get; set; }
        public string prevthurdate { get; set; }
        public string prevfridate { get; set; }
        public string satdate { get; set; }
        public string sundate { get; set; }
        public string mondate { get; set; }
        public string tuedate { get; set; }
        public string weddate { get; set; }
        public string thurdate { get; set; }
        public string fridate { get; set; }
        public int hourconsume { get; set; }
        public DateTime SelectedDate { get; set; }
        public string PrevDateActivity { get; set; }

        public int satsum { get; set; }
        public int sunsum { get; set; }
        public int monsum { get; set; }
        public int tuesum { get; set; }
        public int wedsum { get; set; }
        public int thursum { get; set; }
        public int frisum { get; set; }

        private List<AssignedProject> _AssignProjectList;
        public List<AssignedProject> AssignProjectList
        {
            get { return _AssignProjectList; }
            set
            {
                SetProperty(ref _AssignProjectList, value);
            }
        }

        private List<AssignedProject> _AssignProjectActivities;
        public List<AssignedProject> AssignProjectActivities
        {
            get { return _AssignProjectActivities; }
            set
            {
                SetProperty(ref _AssignProjectActivities, value);
            }
        }

        private List<ProjectModel> _ProjectList;
        public List<ProjectModel> ProjectList
        {
            get { return _ProjectList; }
            set
            {
                SetProperty(ref _ProjectList, value);
            }
        }
    }
}
