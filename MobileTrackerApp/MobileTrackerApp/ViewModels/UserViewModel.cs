﻿using MobileTrackerApp.Models;
using MobileTrackerApp.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobileTrackerApp.ViewModels
{
    public class UserViewModel: BaseViewModel
    {
        public List<UserModel> _userlist;
        public List<UserModel> UserList
        {
            get { return _userlist; }
            set
            {
                SetProperty(ref _userlist, value);
            }
        }

        ApiServices<UserModel> _restClient = new ApiServices<UserModel>();

        public async Task<bool> CheckLoginIfExists(string username, string password)
        {
            var check = await _restClient.checkLogin(username, password);

            return check;
        }
    }
}
