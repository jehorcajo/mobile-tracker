﻿using System;
using System.Collections.Generic;
using System.Text;
using Entry = Microcharts.Entry;
using Xamarin.Forms.Xaml;
using Microcharts;
using SkiaSharp;
using SQLite;
using MobileTrackerApp.Models;

namespace MobileTrackerApp.ViewModels
{
    public class WeeklyViewModel : BaseViewModel
    {
        public string empid { get; set; }
        public DateTime SelectedStartDate { get; set; }
        public DateTime SelectedEndDate { get; set; }
        public int workinghours { get; set; }
        public int totalworkinghours { get; set; }
        public string workinghoursstring { get; set; }
        public int resourcesutilize { get; set; }
        public int utilizedworkhours { get; set; }
        public int leavehours { get; set; }
        public int totalleaveandwork { get; set; }
        public string startenddate { get; set; }
        public int yeartodate { get; set; }


        private string _totalWork;
        public string TotalWork
        {
            get { return _totalWork; }
            set
            {
                _totalWork = value;
                OnPropertyChanged(nameof(TotalWork));
            }
        }

        private string _totalWorkLeaves;
        public string TotalWorkLeaves
        {
            get { return _totalWorkLeaves; }
            set
            {
                _totalWorkLeaves = value;
                OnPropertyChanged(nameof(TotalWorkLeaves));
            }
        }

        private string _mtd;
        public string MTD
        {
            get { return _mtd; }
            set
            {
                _mtd = value;
                OnPropertyChanged(nameof(MTD));
            }
        }

        private string _ytd;
        public string YTD
        {
            get { return _ytd; }
            set
            {
                _ytd = value;
                OnPropertyChanged(nameof(YTD));
            }
        }

        private UtilizationModel _selectedforecasthour;
        public UtilizationModel SelectedForecastHour
        {
            get { return _selectedforecasthour; }
            set
            {
                if (_selectedforecasthour != value)
                {
                    _selectedforecasthour = value;
                    workinghours = _selectedforecasthour.workinghours;
                    startenddate = _selectedforecasthour.startenddate.ToString();
                    SelectedStartDate = _selectedforecasthour.startdate;
                    SelectedEndDate = _selectedforecasthour.enddate;

                    InitializeChart();
                    TotalWork = workinghours.ToString();
                    TotalWorkLeaves = resourcesutilize.ToString();
                    double mtdpercent = (Convert.ToDouble(utilizedworkhours) / Convert.ToDouble(workinghours)) * 100;
                    double ytdpercentage = (Convert.ToDouble(totalworkinghours) / Convert.ToDouble(yeartodate)) * 100;
                    MTD = mtdpercent.ToString() + "%";
                    YTD = Math.Round((Double)ytdpercentage, 2) + "%";
                    OnPropertyChanged(nameof(this.DonutChartSample));
                }
            }
        }

        public WeeklyViewModel(string empIDModels)
        {
            //SelectedStartDate = selectedstartDate;
            //SelectedEndDate = selectedendDate;
            empid = empIDModels;

            InitializeData();
        }

        private void InitializeData()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            {
                conn.CreateTable<UtilizationModel>();
                var forecasthour = conn.Table<UtilizationModel>().ToList();

                Forecast = forecasthour;
            }
        }

        private void InitializeChart()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            {
                conn.CreateTable<ActivityModel>();

                DateTime now = DateTime.Now;
                var datestart = new DateTime(now.Year, 1, 1);
                //var activityrefresh = conn.Query<MyActivitiesTable>("SELECT * FROM MyActivitiesTable WHERE EMPID =? AND ActivityID BETWEEN 2 AND 8 ", empid.ToString());
                var activityrefreshw = conn.Query<UtilizationModel>("SELECT * FROM UtilizationModel");
                var ytd = conn.ExecuteScalar<int>("SELECT IFNULL(SUM(workinghours),0) FROM UtilizationModel WHERE startdate BETWEEN ? AND ? GROUP BY ytdhours", datestart, SelectedEndDate);
                var totalwork = conn.ExecuteScalar<int>("SELECT SUM(ActivityHours) FROM ActivityModel WHERE EMPID =? AND ActivityDate BETWEEN ? AND ? AND withLeaves = 0 GROUP BY EMPID", empid.ToString(), datestart, SelectedEndDate);
                var myactivity = conn.ExecuteScalar<int>("SELECT SUM(ActivityHours) FROM ActivityModel WHERE EMPID =? AND ActivityDate BETWEEN ? AND ? AND withLeaves = 0 GROUP BY EMPID", empid.ToString(), SelectedStartDate, SelectedEndDate);
                var myactivityleaves = conn.ExecuteScalar<int>("SELECT SUM(ActivityHours) FROM ActivityModel WHERE EMPID =? AND ActivityDate BETWEEN ? AND ? AND withLeaves = 1 GROUP BY EMPID", empid.ToString(), SelectedStartDate, SelectedEndDate);

                totalworkinghours = totalwork;
                yeartodate = ytd;
                utilizedworkhours = myactivity;
                leavehours = myactivityleaves;
                totalleaveandwork = workinghours - (utilizedworkhours + leavehours);
                resourcesutilize = workinghours - leavehours;
            }
        }

        private List<ActivityModel> _MyActivities;
        public List<ActivityModel> MyActivities
        {
            get { return _MyActivities; }
            set
            {
                SetProperty(ref _MyActivities, value);
            }
        }
        private List<UtilizationModel> _Forecast;
        public List<UtilizationModel> Forecast
        {
            get { return _Forecast; }
            set
            {
                SetProperty(ref _Forecast, value);
            }
        }
        public Chart DonutChartSample => new DonutChart()
        {
            Entries = new[]
            {
                new Entry(leavehours)
                {
                    Color = SKColor.Parse("#FF1943"),
                    Label = "Leaves Utilized",
                    TextColor = SKColor.Parse("#FF1943"),
                    ValueLabel = leavehours.ToString() + " Hours"
                },
                new Entry(utilizedworkhours)
                {
                    Color = SKColor.Parse("#4682B4"),
                    Label = "Work Utilized",
                    TextColor = SKColor.Parse("#4682B4"),
                    ValueLabel = utilizedworkhours.ToString() + " Hours"
                },
                new Entry(totalleaveandwork)
                {
                    Color = SKColor.Parse("#008000"),
                    Label = "Remaining Work Hours",
                    TextColor = SKColor.Parse("#008000"),
                    ValueLabel = totalleaveandwork.ToString() + " Hours"
                }
            },
            LabelTextSize = 25,
            HoleRadius = 0.50f
        };
    }
}
