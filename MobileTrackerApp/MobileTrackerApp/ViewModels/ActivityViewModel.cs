﻿using MobileTrackerApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileTrackerApp.ViewModels
{
    public class ActivityViewModel : BaseViewModel
    {
        //private List<ActivityModel> _ActivityModel;
        //public List<ActivityModel> Activitymodel
        //{
        //    get { return _ActivityModel; }
        //    set
        //    {
        //        SetProperty(ref _ActivityModel, value);
        //    }
        //}
        //private List<ActivityModel> _ActModelSun;
        //public List<ActivityModel> ActModelSun
        //{
        //    get { return _ActModelSun; }
        //    set
        //    {
        //        SetProperty(ref _ActModelSun, value);
        //    }
        //}

        //private List<ActivityModel> _ActModelSat;
        //public List<ActivityModel> ActModelSat
        //{
        //    get { return _ActModelSat; }
        //    set
        //    {
        //        SetProperty(ref _ActModelSat, value);
        //    }
        //}

        //private List<AssignedProject> _AssProject;
        //public List<AssignedProject> AssProject
        //{
        //    get { return _AssProject; }
        //    set
        //    {
        //        SetProperty(ref _AssProject, value);
        //    }
        //}

        //private List<ProjectModel> _Projectmodel;
        //public List<ProjectModel> Projectmodel
        //{
        //    get { return _Projectmodel; }
        //    set
        //    {
        //        SetProperty(ref _Projectmodel, value);
        //    }
        //}

        //private List<BillCodeModel> _BillCodeModel;
        //public List<BillCodeModel> Billcodemodel
        //{
        //    get { return _BillCodeModel; }
        //    set
        //    {
        //        SetProperty(ref _BillCodeModel, value);
        //    }
        //}
        //private string _myProject;
        //public string myProject
        //{
        //    get { return _myProject; }
        //    set
        //    {
        //        if (_myProject != value)
        //        {
        //            _myProject = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private string _myProjectCode;
        //public string myProjectCode
        //{
        //    get { return _myProjectCode; }
        //    set
        //    {
        //        if (_myProjectCode != value)
        //        {
        //            _myProjectCode = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private string _myProjectDesc;
        //public string myProjectDesc
        //{
        //    get { return _myProjectDesc; }
        //    set
        //    {
        //        if (_myProjectDesc != value)
        //        {
        //            _myProjectDesc = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private string _myProjectCompany;
        //public string myProjectCompany
        //{
        //    get { return _myProjectCompany; }
        //    set
        //    {
        //        if (_myProjectCompany != value)
        //        {
        //            _myProjectCompany = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private string _myAssignedProject;
        //public string myAssignedProject
        //{
        //    get { return _myAssignedProject; }
        //    set
        //    {
        //        if (_myAssignedProject != value)
        //        {
        //            _myAssignedProject = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private string _myAssignedProjectCode;
        //public string myAssignedProjectCode
        //{
        //    get { return _myAssignedProjectCode; }
        //    set
        //    {
        //        if (_myAssignedProjectCode != value)
        //        {
        //            _myAssignedProjectCode = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private string _myAssignedProjectDesc;
        //public string myAssignedProjectDesc
        //{
        //    get { return _myAssignedProjectDesc; }
        //    set
        //    {
        //        if (_myAssignedProjectDesc != value)
        //        {
        //            _myAssignedProjectDesc = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private string _myAssignedProjectCompany;
        //public string myAssignedProjectCompany
        //{
        //    get { return _myAssignedProjectCompany; }
        //    set
        //    {
        //        if (_myAssignedProjectCompany != value)
        //        {
        //            _myAssignedProjectCompany = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}
        //private string _myBillCode;
        //public string myBillCode
        //{
        //    get { return _myBillCode; }
        //    set
        //    {
        //        if (_myBillCode != value)
        //        {
        //            _myBillCode = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private string _myBillCodeDesc;
        //public string myBillCodeDesc
        //{
        //    get { return _myBillCodeDesc; }
        //    set
        //    {
        //        if (_myBillCodeDesc != value)
        //        {
        //            _myBillCodeDesc = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private BillCodeModel _selectedBillCode;
        //public BillCodeModel SelectedBillCode
        //{
        //    get { return _selectedBillCode; }
        //    set
        //    {
        //        if (_selectedBillCode != value)
        //        {
        //            _selectedBillCode = value;
        //            myBillCode = _selectedBillCode.BillCode.ToString();
        //            myBillCodeDesc = _selectedBillCode.BillDesc.ToString();
        //        }
        //    }
        //}
        //private AssignedProject _selectedAssignedProject;
        //public AssignedProject SelectedAssignedProject
        //{
        //    get { return _selectedAssignedProject; }
        //    set
        //    {
        //        if (_selectedAssignedProject != value)
        //        {
        //            _selectedAssignedProject = value;
        //            myAssignedProject = _selectedAssignedProject.ProjectName.ToString();
        //            myAssignedProjectCode = _selectedAssignedProject.ProjectCode.ToString();
        //            myAssignedProjectDesc = _selectedAssignedProject.ProjectDesc.ToString();
        //            myAssignedProjectCompany = _selectedAssignedProject.CompanyCode.ToString();
        //        }
        //    }
        //}
        //private ProjectModel _selectedProject;
        //public ProjectModel SelectedProject
        //{
        //    get { return _selectedProject; }
        //    set
        //    {
        //        if (_selectedProject != value)
        //        {
        //            _selectedProject = value;
        //            myProject = _selectedProject.ProjectName.ToString();
        //            myProjectCode = _selectedProject.ProjectCode.ToString();
        //            myProjectDesc = _selectedProject.ProjectDesc.ToString();
        //            myProjectCompany = _selectedProject.CompanyCode.ToString();
        //        }
        //    }
        //}
        //public string EmpID { get; set; }
        //public ActivityViewModel(UserModel usermodel)
        //{
        //    EmpID = usermodel.EmpID;
        //    AssProject = new List<AssignedProject>();
        //    Billcodemodel = new List<BillCodeModel>();

        //    InitializeData();
        //}
        //public ObservableCollection<ActivityModel> MyActivityList { get; set; }
        //private ActivityModel _SelectedActivities;
        //public ActivityModel SelectedActivities
        //{
        //    get { return _SelectedActivities; }
        //    set
        //    {
        //        if (_SelectedActivities != value)
        //        {
        //            _SelectedActivities = value;
        //        }
        //    }
        //}
        //public ActivityViewModel(string empidmodels)
        //{
        //    empid = empidmodels;
        //    AssProject = new List<AssignedProject>();
        //    Billcodemodel = new List<BillCodeModel>();


        //    InitializeData();
        //}
        //private void InitializeData()
        //{
        //    PopulateDate();
        //    using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
        //    {
        //        conn.CreateTable<AssignedProject>();
        //        var assignprojectlist = conn.Table<AssignedProject>().ToList();

        //        conn.CreateTable<AssignedProject>();
        //        var assignprojectlistActivities = conn.Table<AssignedProject>().ToList();

        //        conn.CreateTable<BillCodeModel>();
        //        var billdesclist = conn.Table<BillCodeModel>().ToList();

        //        conn.CreateTable<ProjectModel>();
        //        var projectlist = conn.Table<ProjectModel>().ToList();

        //        conn.CreateTable<ActivityModel>();
        //        var satlist = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), satdate.ToString());
        //        var sunlist = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), sundate.ToString());

        //        ActModelSun = sunlist;
        //        ActModelSat = satlist;

               
        //        //AssignProjectActivities = assignprojectlistActivities;
        //        Billcodemodel = billdesclist;
        //        AssProject = assignprojectlist;
        //        Projectmodel = projectlist;
        //    }
        //}
        //public string empid { get; set; }
        //public DateTime satdate { get; set; }
        //public DateTime sundate { get; set; }
        //public DateTime mondate { get; set; }
        //public DateTime tuedate { get; set; }
        //public DateTime weddate { get; set; }
        //public DateTime thurdate { get; set; }
        //public DateTime fridate { get; set; }
        //public DateTime hourconsume { get; set; }
        //public void PopulateDate()
        //{
        //    DayOfWeek day = DateTime.Now.DayOfWeek;
        //    int days = day - DayOfWeek.Monday + 2;
        //    DateTime start = DateTime.Now.AddDays(-days);
        //    satdate = Convert.ToDateTime(start.ToString("MMM d, yyyy"));
        //    sundate = Convert.ToDateTime(start.AddDays(1).ToString("MMM d, yyyy"));
        //    mondate = Convert.ToDateTime(start.AddDays(2).ToString("MMM d, yyyy"));
        //    tuedate = Convert.ToDateTime(start.AddDays(3).ToString("MMM d, yyyy"));
        //    weddate = Convert.ToDateTime(start.AddDays(4).ToString("MMM d, yyyy"));
        //    thurdate = Convert.ToDateTime(start.AddDays(5).ToString("MMM d, yyyy"));
        //    fridate = Convert.ToDateTime(start.AddDays(6).ToString("MMM d, yyyy"));
        //}

        //public ICommand CommandAddSat
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {
        //            ActivityModel activity = new ActivityModel()
        //            {
        //                EMPID = empid.ToString(),
        //                ProjectCode = myAssignedProjectCode,
        //                ActivityName = myAssignedProject,
        //                ActivityDesc = myAssignedProjectDesc,
        //                ActivityDate = Convert.ToDateTime(satdate.ToString("MMM dd, yyyy")),
        //                BillCode = myBillCode,
        //                CompanyCode = myAssignedProjectCompany
        //            };

        //            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
        //            {
        //                conn.CreateTable<ActivityModel>();
        //                var rowcount = conn.Insert(activity);

        //                conn.CreateTable<ActivityModel>();
        //                var satlist = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), satdate.ToString());
        //                ActModelSat = satlist;
        //            }

        //            await App.Current.MainPage.DisplayAlert("", "You Successfully Added New Activity", "OK");
        //        });
        //    }
        //}

        //public ICommand CommandAddSun
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {

        //            ActivityModel activity = new ActivityModel()
        //            {
        //                EMPID = empid.ToString(),

        //                ProjectCode = myAssignedProjectCode,
        //                ActivityName = myAssignedProject,
        //                ActivityDesc = myAssignedProjectDesc,
        //                ActivityDate = Convert.ToDateTime(sundate.ToString("MMM dd, yyyy")),
        //                BillCode = myBillCode,
        //                CompanyCode = myAssignedProjectCompany
        //            };

        //            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
        //            {
        //                conn.CreateTable<ActivityModel>();
        //                var rowcount = conn.Insert(activity);

        //                conn.CreateTable<ActivityModel>();

        //                var sunlist = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), sundate.ToString());

        //                ActModelSun = sunlist;
        //            }
        //            PopulateDate();

        //            await App.Current.MainPage.DisplayAlert("", "You Successfully Added New Activity", "OK");
        //        });
        //    }
        //}



    }
}
