﻿using MobileTrackerApp.Models;
using MobileTrackerApp.Services;
using MobileTrackerApp.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileTrackerApp.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private SQLiteConnection conn;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        [PrimaryKey]
        public int Id { get; set; }

        public string empid;

        public string EmpID
        {
            get { return empid; }
            set
            {
                empid = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EmpID"));
            }
        }

        public string rememberme;
        public string Checkme
        {
            get { return rememberme; }
            set
            {
                rememberme = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Checkme"));
            }
        }

        public string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Password"));
            }
        }

       

        public ICommand SubmitLogin { get; set; }

        public LoginViewModel()
        {

        }

        public ICommand OnSubmitCommand
        {
            get
            {
                return new Command(() =>
                {

                    UserModel usermodel = new UserModel();
                    if (string.IsNullOrEmpty(EmpID) || string.IsNullOrEmpty(Password))
                    {
                        MessagingCenter.Send(this, "LoginAlert", "Please fill empty textbox.");
                       
                    }
                    else
                    {
                        using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                        {
                            conn.CreateTable<UserModel>();
                            var data = conn.Table<UserModel>().ToList();
                            
                            //var saltpassword = Convert.ToBase64String(
                            //Common.SaltHashPassword(Encoding.ASCII.GetBytes(Password),
                            //    Convert.FromBase64String(Password)));

                            var crypt = Common.CryptoHash(Password);
                            var saltpassword = crypt.Item2.ToString();


                            var emp = EmpID.ToUpper();
                            //var data = conn.Table<EmployeeModel>();
                            var d1 = data.Where(x => x.EmpID == emp && x.HashPassword == saltpassword).FirstOrDefault();
                            if (d1 != null)
                            {
                               
                                var settingmodel = conn.Table<SettingModel>().ToList();
                                int count = settingmodel.Count();
                                if (Checkme == "true")
                                {
                                    if (count == 0)
                                    {
                                        SettingModel model = new SettingModel()
                                        {
                                            settingID = 1,
                                            rememberme = true,
                                            rememberempid = EmpID,
                                            rememberpass = Password
                                        };
                                        using (SQLiteConnection con = new SQLiteConnection(App.FilePath))
                                        {
                                            conn.CreateTable<SettingModel>();
                                            var rowcount = conn.Insert(model);
                                        }
                                    }
                                    else
                                    {
                                        SettingModel model = new SettingModel()
                                        {
                                            settingID = 1,
                                            rememberme = true,
                                            rememberempid = EmpID,
                                            rememberpass = Password
                                        };
                                        using (SQLiteConnection con = new SQLiteConnection(App.FilePath))
                                        {
                                            conn.CreateTable<SettingModel>();
                                            var rowcount = conn.Update(model);
                                        }
                                    }
                                }
                                else
                                {
                                    SettingModel model = new SettingModel()
                                    {
                                        settingID = 1,
                                        rememberme = false
                                    };
                                    using (SQLiteConnection con = new SQLiteConnection(App.FilePath))
                                    {
                                        conn.CreateTable<SettingModel>();
                                        var rowcount = conn.Update(model);
                                    }
                                }
                                
                                App.Current.MainPage = new NavigationPage(new Dashboard(d1.EmpID));
                                //MessagingCenter.Send(this, "LoginAlert", "Welcome " + d1.FirstName.ToString() + " " + d1.LastName.ToString());
                                //Convert.ToInt32(d1.EMPID.ToString()))
                            }
                            else
                                MessagingCenter.Send(this, "LoginAlert", "Invalid Credential");

                        }
                    }
                });
            }
        }
    }
}
