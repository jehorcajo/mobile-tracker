﻿using MobileTrackerApp.Models;
using System;
using SQLite;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using Entry = Microcharts.Entry;
using Xamarin.Forms.Xaml;
using Microcharts;
using SkiaSharp;
using System.Net.Http;
using Newtonsoft.Json;

namespace MobileTrackerApp.ViewModels
{
    public class AssignedActivityViewModel : BaseViewModel
    {
        public string empid { get; set; }
        public string prevsatdate { get; set; }
        public string prevsundate { get; set; }
        public string prevmondate { get; set; }
        public string prevtuedate { get; set; }
        public string prevweddate { get; set; }
        public string prevthurdate { get; set; }
        public string prevfridate { get; set; }
        public string satdate { get; set; }
        public string sundate { get; set; }
        public string mondate { get; set; }
        public string tuedate { get; set; }
        public string weddate { get; set; }
        public string thurdate { get; set; }
        public string fridate { get; set; }
        public int hourconsume { get; set; }
        public DateTime SelectedDate { get; set; }
        public string PrevDateActivity { get; set; }
        public int satsum { get; set; }
        public int sunsum { get; set; }
        public int monsum { get; set; }
        public int tuesum { get; set; }
        public int wedsum { get; set; }
        public int thursum { get; set; }
        public int frisum { get; set; }

        private string _myBillCode;
        public string myBillCode
        {
            get { return _myBillCode; }
            set
            {
                if (_myBillCode != value)
                {
                    _myBillCode = value;
                    OnPropertyChanged();
                }
            }
        }

        //    private string _ActivityID;
        //    public string ActivityID
        //    {
        //        get { return _ActivityID; }
        //        set
        //        {
        //            if (_ActivityID != value)
        //            {
        //                _ActivityID = value;
        //                OnPropertyChanged();
        //            }
        //        }
        //    }

        //    private string _myActivityDesc;
        //    public string myActivityDesc
        //    {
        //        get { return _myActivityDesc; }
        //        set
        //        {
        //            if (_myActivityDesc != value)
        //            {
        //                _myActivityDesc = value;
        //                OnPropertyChanged();
        //            }
        //        }
        //    }

        private string _myBillCodeDesc;
        public string myBillCodeDesc
        {
            get { return _myBillCodeDesc; }
            set
            {
                if (_myBillCodeDesc != value)
                {
                    _myBillCodeDesc = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _myAssignedProject;
        public string myAssignedProject
        {
            get { return _myAssignedProject; }
            set
            {
                if (_myAssignedProject != value)
                {
                    _myAssignedProject = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _myAssignedProjectCode;
        public string myAssignedProjectCode
        {
            get { return _myAssignedProjectCode; }
            set
            {
                if (_myAssignedProjectCode != value)
                {
                    _myAssignedProjectCode = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _myAssignedProjectDesc;
        public string myAssignedProjectDesc
        {
            get { return _myAssignedProjectDesc; }
            set
            {
                if (_myAssignedProjectDesc != value)
                {
                    _myAssignedProjectDesc = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _myAssignedProjectCompany;
        public string myAssignedProjectCompany
        {
            get { return _myAssignedProjectCompany; }
            set
            {
                if (_myAssignedProjectCompany != value)
                {
                    _myAssignedProjectCompany = value;
                    OnPropertyChanged();
                }
            }
        }

        private List<AssignedProject> _AssignProjectList;
        public List<AssignedProject> AssignProjectList
        {
            get { return _AssignProjectList; }
            set
            {
                SetProperty(ref _AssignProjectList, value);
            }
        }

        //    private List<AssignedProject> _AssignProjectActivities;
        //    public List<AssignedProject> AssignProjectActivities
        //    {
        //        get { return _AssignProjectActivities; }
        //        set
        //        {
        //            SetProperty(ref _AssignProjectActivities, value);
        //        }
        //    }

        private List<ProjectModel> _ProjectList;
        public List<ProjectModel> ProjectList
        {
            get { return _ProjectList; }
            set
            {
                SetProperty(ref _ProjectList, value);
            }
        }

        private List<BillCodeModel> _BillDescList;
        public List<BillCodeModel> BillDescList
        {
            get { return _BillDescList; }
            set
            {
                SetProperty(ref _BillDescList, value);
            }
        }

        private List<ActivityModel> _MyActivities;
        public List<ActivityModel> MyActivities
        {
            get { return _MyActivities; }
            set
            {
                SetProperty(ref _MyActivities, value);
            }
        }
        //    public Command<object> DeleteCommand { get; set; }

        private BillCodeModel _selectedBillCode;
        public BillCodeModel SelectedBillCode
        {
            get { return _selectedBillCode; }
            set
            {
                if (_selectedBillCode != value)
                {
                    _selectedBillCode = value;
                    myBillCode = _selectedBillCode.BillCode.ToString();
                    myBillCodeDesc = _selectedBillCode.BillDesc.ToString();
                }
            }
        }

        //    private ActivityModel _selectedActivities;
        //    public ActivityModel selectedActivities
        //    {
        //        get { return _selectedActivities; }
        //        set
        //        {
        //            if (_selectedActivities != value)
        //            {
        //                _selectedActivities = value;
        //                ActivityID = _selectedActivities.ActivityID.ToString();
        //            }
        //        }
        //    }

        private AssignedProject _selectedAssignedProject;
        public AssignedProject SelectedAssignedProject
        {
            get { return _selectedAssignedProject; }
            set
            {
                if (_selectedAssignedProject != value)
                {
                    _selectedAssignedProject = value;
                    myAssignedProject = _selectedAssignedProject.ProjectName.ToString();
                    myAssignedProjectCode = _selectedAssignedProject.ProjectCode.ToString();
                    myAssignedProjectDesc = _selectedAssignedProject.ProjectDesc.ToString();
                    myAssignedProjectCompany = _selectedAssignedProject.CompanyCode.ToString();
                }
            }
        }

        public AssignedActivityViewModel(string empIDModels, string selectedDate)
        {
            SelectedDate = Convert.ToDateTime(selectedDate);
            empid = empIDModels;
            DeleteCommand = new Command<object>(OnTapped);

            InitializeData();
        }

        bool _ConDelete = true;
        private async void OnTapped(object obj)
        {
            if (_ConDelete)
            {
                var answer = await App.Current.MainPage.DisplayAlert("Warning", "Do you want to Delete?", "Yes", "No");
                if (answer)
                {
                    var contact = obj as ActivityModel;
                    ActivityModel activitydelete = new ActivityModel()
                    {
                        EMPID = empid.ToString(),
                        ActivityID = contact.ActivityID
                    };
                    using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                    {
                        conn.CreateTable<ActivityModel>();
                        var rowcount = conn.Delete<ActivityModel>(contact.ActivityID);
                        var activityrefresh = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), SelectedDate);
                        MyActivities = activityrefresh;
                    
                    }
                    HttpClient _client = new HttpClient();

                    var json = JsonConvert.SerializeObject(activitydelete);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = _client.PostAsync("http://192.168.137.1:5003/api/DeleteActivity", content);

                    await App.Current.MainPage.DisplayAlert("Message", "Item Deleted :" + contact.ActivityName, "Ok");


                }
            }
        }

        private void InitializeData()
        {
            PopulateDate();
            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            {
                conn.CreateTable<AssignedProject>();
                var assignprojectlist = conn.Table<AssignedProject>().ToList();

                conn.CreateTable<AssignedProject>();
                var assignprojectlistActivities = conn.Table<AssignedProject>().ToList();

                conn.CreateTable<BillCodeModel>();
                //var billdesclist = conn.Table<BillDescTable>().ToList();
                var billdesclist = conn.Query<BillCodeModel>("SELECT * FROM BillCodeModel WHERE IsProductive = 1 OR IsBillable=1 ");

                conn.CreateTable<ProjectModel>();
                var projectlist = conn.Table<ProjectModel>().ToList();

                var vt = String.Format("{0:MMM dd, yyyy}", SelectedDate);

                conn.CreateTable<ActivityModel>();
                var myactivity = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), SelectedDate);

                MyActivities = myactivity;

                BillDescList = billdesclist;
                //AssignProjectActivities = assignprojectlistActivities;
                AssignProjectList = assignprojectlist;
                ProjectList = projectlist;
            }
        }

        public void PopulateDate()
        {
            DayOfWeek day = DateTime.Now.DayOfWeek;
            int days = day - DayOfWeek.Monday + 2;
            DateTime start = DateTime.Now.Date.AddDays(-days);

            prevsatdate = start.AddDays(-7).ToString("MMM dd, yyyy");
            DateTime psat = start.AddDays(-7);
            string g = psat.ToShortDateString();
            prevsundate = start.AddDays(-6).ToString("MMM dd, yyyy");
            prevmondate = start.AddDays(-5).ToString("MMM dd, yyyy");
            prevtuedate = start.AddDays(-4).ToString("MMM dd, yyyy");
            prevweddate = start.AddDays(-3).ToString("MMM dd, yyyy");
            prevthurdate = start.AddDays(-2).ToString("MMM dd, yyyy");
            prevfridate = start.AddDays(-1).ToString("MMM dd, yyyy");
            satdate = start.ToString("MMM dd, yyyy");
            sundate = start.AddDays(1).ToString("MMM dd, yyyy");
            mondate = start.AddDays(2).ToString("MMM dd, yyyy");
            tuedate = start.AddDays(3).ToString("MMM dd, yyyy");
            weddate = start.AddDays(4).ToString("MMM dd, yyyy");
            thurdate = start.AddDays(5).ToString("MMM dd, yyyy");
            fridate = start.AddDays(6).ToString("MMM dd, yyyy");
            PrevDateActivity = prevsatdate.ToString() + " to " + prevfridate.ToString();

            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            {
                conn.CreateTable<ActivityModel>();
                var activityrefresh = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate= datetime('2019-08-17','YYYY-MM-DD') ", empid.ToString());
                var activityrefresh2 = conn.Query<ActivityModel>("SELECT * FROM ActivityModel");
                var avgsat = conn.ExecuteScalar<int>("SELECT IFNULL(SUM(ActivityHours),0) FROM ActivityModel WHERE EMPID =? AND ActivityDate=?", empid.ToString(), Convert.ToDateTime(start.AddDays(-7)));
                var avgsun = conn.ExecuteScalar<int>("SELECT IFNULL(SUM(ActivityHours),0) FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), Convert.ToDateTime(start.AddDays(-6)));
                var avgmon = conn.ExecuteScalar<int>("SELECT IFNULL(SUM(ActivityHours),0) FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), Convert.ToDateTime(start.AddDays(-5)));
                var avgtue = conn.ExecuteScalar<int>("SELECT IFNULL(SUM(ActivityHours),0) FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), Convert.ToDateTime(start.AddDays(-4)));
                var avgwed = conn.ExecuteScalar<int>("SELECT IFNULL(SUM(ActivityHours),0) FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), Convert.ToDateTime(start.AddDays(-3)));
                var avgthur = conn.ExecuteScalar<int>("SELECT IFNULL(SUM(ActivityHours),0) FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), Convert.ToDateTime(start.AddDays(-2)));
                var avgfri = conn.ExecuteScalar<int>("SELECT IFNULL(SUM(ActivityHours),0) FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), Convert.ToDateTime(start.AddDays(-1)));
                satsum = avgsat;
                sunsum = avgsun;
                monsum = avgmon;
                tuesum = avgtue;
                wedsum = avgwed;
                thursum = avgthur;
                frisum = avgfri;
            }
        }

        public ICommand CommandAddActivity
        {
            get
            {
                return new Command(async () =>
                {
                    ActivityModel activity = new ActivityModel()
                    {
                        withLeaves = false,
                        EMPID = empid.ToString(),
                        ProjectCode = myAssignedProjectCode,
                        ActivityName = myAssignedProject,
                        ActivityDesc = myAssignedProjectDesc,
                        ActivityDate = SelectedDate, //String.Format("{ 0:MMM dd, yyyy}", SelectedDate),
                        ActivityHours = hourconsume,
                        BillCode = myBillCode,
                        CompanyCode = myAssignedProjectCompany
                    };

                    using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                    {
                        conn.CreateTable<ActivityModel>();
                        var rowcount = conn.Insert(activity);

                        var activityrefresh2 = conn.Query<ActivityModel>("SELECT * FROM ActivityModel");
                        var activityrefresh = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), SelectedDate);
                        MyActivities = activityrefresh;
                    }

                    HttpClient _client = new HttpClient();

                    var json = JsonConvert.SerializeObject(activity);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = _client.PostAsync("http://192.168.137.1:5003/api/PostActivity", content);

                    await Application.Current.MainPage.DisplayAlert("", "You Successfully Added New Activity", "OK");
                });
            }
        }
        public Command<object> DeleteCommand { get; set; }
        //    public ICommand DeleteCommands
        //    {
        //        get
        //        {
        //            return new Command(async () =>
        //            {
        //                using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
        //                {
        //                    conn.CreateTable<ActivityModel>();
        //                    var rowcount = conn.Delete<ActivityModel>(selectedActivities.ActivityID);
        //                    var activityrefresh = conn.Query<ActivityModel>("SELECT * FROM ActivityModel WHERE EMPID =? AND ActivityDate=? ", empid.ToString(), SelectedDate);
        //                    MyActivities = activityrefresh;
        //                }
        //                await App.Current.MainPage.DisplayAlert("", "You Successfully Delete Activity", "OK");
        //            });
        //        }
        //    }

        public Chart BarChartSample => new BarChart()
        {
            Entries = new[]
            {
                     new Entry(satsum)
                     {
                        Color = SKColor.Parse("#ff0000"),
                        Label = "Saturday",
                        ValueLabel = satsum.ToString() + " Hours"
                     },
                     new Entry(sunsum)
                     {
                        Color = SKColor.Parse("#ff8000"),
                        Label = "Sunday",
                        ValueLabel = sunsum.ToString() + " Hours"
                     },
                     new Entry(monsum)
                     {
                        Color = SKColor.Parse("#993300"),
                        Label = "Monday",
                        ValueLabel = monsum.ToString() + " Hours"
                     },
                     new Entry(tuesum)
                     {
                        Color = SKColor.Parse("#008000"),
                        Label = "Tuesday",
                        ValueLabel = tuesum.ToString() + " Hours"
                     },
                     new Entry(wedsum)
                     {
                        Color = SKColor.Parse("#0000ff"),
                        Label = "Wednesday",
                        ValueLabel = wedsum.ToString() + " Hours"
                     },
                     new Entry(thursum)
                     {
                        Color = SKColor.Parse("#ff00bf"),
                        Label = "Thursday",
                        ValueLabel = thursum.ToString() + " Hours"
                     },
                     new Entry(frisum)
                     {
                        Color = SKColor.Parse("#0000cc"),
                        Label = "Friday",
                        ValueLabel = frisum.ToString() + " Hours"
                     }
                },
            PointMode = PointMode.Circle,
            //LineSize = 3,
            //LineAreaAlpha = 10,
            PointSize = 20,
            //LineMode = LineMode.Straight,
            BackgroundColor = SKColor.Parse("#D8D8D8"),
            LabelTextSize = 25
        };
    }
}
