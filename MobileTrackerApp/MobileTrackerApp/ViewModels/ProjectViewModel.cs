﻿using MobileTrackerApp.Models;
using MobileTrackerApp.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileTrackerApp.ViewModels
{
    public class ProjectViewModel : BaseViewModel
    {
        public ProjectViewModel()
        {
            ProjectList = new List<ProjectModel>();
            InitializeData();
        }
        private void InitializeData()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            {
                conn.CreateTable<ProjectModel>();
                var projlist = conn.Table<ProjectModel>().ToList();

                ProjectList = projlist;
            }
        }

        private List<ProjectModel> _ProjectList;
        public List<ProjectModel> ProjectList
        {
            get { return _ProjectList; }
            set
            {
                SetProperty(ref _ProjectList, value);
            }
        }
        private string _ProjectNameText;
        public string ProjectNameText
        {
            get { return _ProjectNameText; }
            set
            {
                SetProperty(ref _ProjectNameText, value);
            }
        }

        private string _ProjectCodeText;
        public string ProjectCodeText
        {
            get { return _ProjectCodeText; }
            set
            {
                SetProperty(ref _ProjectCodeText, value);
            }
        }

        private string _ProjectDescText;
        public string ProjectDescText
        {
            get { return _ProjectDescText; }
            set
            {
                SetProperty(ref _ProjectDescText, value);
            }
        }

        private string _CompanyCodeText;
        public string CompanyCodeText
        {
            get { return _CompanyCodeText; }
            set
            {
                SetProperty(ref _CompanyCodeText, value);
            }
        }

    }
}
