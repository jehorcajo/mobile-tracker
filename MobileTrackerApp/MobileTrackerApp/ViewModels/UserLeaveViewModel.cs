﻿using MobileTrackerApp.Models;
using MobileTrackerApp.Views;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobileTrackerApp.ViewModels
{
    public class UserLeaveViewModel : BaseViewModel
    {
        private List<LeavesModel> _LeaveModelList;
        public List<LeavesModel> LeaveModelList
        {
            get { return _LeaveModelList; }
            set
            {
                SetProperty(ref _LeaveModelList, value);
            }
        }

        private string _myLeaves;
        public string MyLeaves
        {
            get { return _myLeaves; }
            set
            {
                if (_myLeaves != value)
                {
                    _myLeaves = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _myLeaveDesc;
        public string MyLeaveDesc
        {
            get { return _myLeaveDesc; }
            set
            {
                if (_myLeaveDesc != value)
                {
                    _myLeaveDesc = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _myBillCode;
        public string MyBillCode
        {
            get { return _myBillCode; }
            set
            {
                if (_myBillCode != value)
                {
                    _myBillCode = value;
                    OnPropertyChanged();
                }
            }
        }

        private LeavesModel _selectedLeavedesc;
        public LeavesModel SelectedLeaves
        {
            get { return _selectedLeavedesc; }
            set
            {
                if (_selectedLeavedesc != value)
                {
                    _selectedLeavedesc = value;
                    MyBillCode = _selectedLeavedesc.BillCode.ToString();
                    MyLeaves = _selectedLeavedesc.LeaveType.ToString();
                    MyLeaveDesc = _selectedLeavedesc.LeaveDesc.ToString();
                }
            }
        }

        public string empid { get; set; }
        public int ActID { get; set; }

        private int _time;

        public int Time
        {
            get { return _time; }
            set
            {
                SetProperty(ref _time, value);
            }
        }

        private DateTime _Schedule;

        public DateTime Schedule
        {
            get { return _Schedule; }
            set
            {
                SetProperty(ref _Schedule, value);
            }
        }

        public UserLeaveViewModel()
        {
            LeaveModelList = new List<LeavesModel>();

            InitializeData();

            Schedule = DateTime.Now;
        }

        private void InitializeData()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            {
                conn.CreateTable<LeavesModel>();
                var leavedesc = conn.Table<LeavesModel>().ToList();

                LeaveModelList = leavedesc;
            }
        }

        public ICommand AddCommand
        {
            get
            {
                return new Command(async () =>
                {
                    //empid.ToLower();
                    ActivityModel leavelist = new ActivityModel()
                    {
                        withLeaves = true,
                        EMPID = empid.ToLower(),
                        LeaveType = MyLeaves,
                        ActivityName = MyLeaves,
                        BillCode = MyBillCode,
                        ActivityHours = Convert.ToInt32(Time),
                        ActivityDate = Convert.ToDateTime(Schedule.ToString("MMM dd, yyyy")),
                        LeaveDesc = MyLeaveDesc
                    };

                    using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                    {
                        conn.CreateTable<ActivityModel>();
                        var rowcount = conn.Insert(leavelist);
                    }

                    HttpClient _client = new HttpClient();

                    var json = JsonConvert.SerializeObject(leavelist);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = _client.PostAsync("http://192.168.137.1:5003/api/PostActivity", content);

                    await App.Current.MainPage.DisplayAlert("", "Leave Added Successfully", "OK");
                    App.Current.MainPage = new UserLeaves(empid);
                });
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                return new Command(async () =>
                {
                    //await App.Current.MainPage.DisplayAlert("Information", MyText, "Okay");
                });
            }
        }
        public ICommand UpdateCommand
        {
            get
            {
                return new Command(async () =>
                {
                    ActivityModel leavelist = new ActivityModel()
                    {
                        withLeaves = true,
                        EMPID = empid.ToLower(),
                        ActivityID = ActID,
                        LeaveType = MyLeaves,
                        ActivityHours = Convert.ToInt32(Time),
                        ActivityDate = Convert.ToDateTime(Schedule.ToString("MMM d, yyyy")),
                        LeaveDesc = MyLeaveDesc
                    };

                    using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                    {
                        conn.CreateTable<ActivityModel>();
                        var rowcount = conn.Update(leavelist);
                    }

                    await App.Current.MainPage.DisplayAlert("", "Update Succesfull", "OK");
                    App.Current.MainPage = new UserLeaves(empid);
                });
            }
        }
    }
}
