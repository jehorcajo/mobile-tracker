﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MobileTrackerApp.Services;
using MobileTrackerApp.Views;

namespace MobileTrackerApp
{
    public partial class App : Application
    {
        public static string FilePath = "/data/user/0/com.companyname.MobileTrackerApp/files/etracker_db.db3";
        public App()
        {
            InitializeComponent();

            //DependencyService.Register<MockDataStore>();
            MainPage = new LoadingScreen();
            //MainPage = new MainPage();
        }
        public App(string filepath)
        {
            InitializeComponent();

            //MainPage = new Login();
            MainPage = new LoadingScreen();

            FilePath = filepath;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
