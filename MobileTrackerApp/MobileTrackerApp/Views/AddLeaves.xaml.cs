﻿using MobileTrackerApp.Models;
using MobileTrackerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddLeaves : ContentPage
    {
        string empid;
        UserLeaveViewModel _viewmodel;
        public AddLeaves(string employeeid)
        {
            InitializeComponent();
            BindingContext = _viewmodel = new UserLeaveViewModel();
            _viewmodel.empid = employeeid;
            empid = employeeid;
        }
        protected override bool OnBackButtonPressed()
        {
            App.Current.MainPage = new NavigationPage(new MobileTrackerApp.Views.UserLeaves(empid));
            return true;
        }

    }
}