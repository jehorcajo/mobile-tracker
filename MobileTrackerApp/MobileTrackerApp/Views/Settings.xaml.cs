﻿using MobileTrackerApp.Services;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Settings : ContentPage
    {
        string constatus;
        public Settings()
        {
            InitializeComponent();
            connectionstatus();
        }

        public async void connectionstatus()
        {
            HttpClient client = new HttpClient();
            
            if (CrossConnectivity.Current.IsConnected)
            {
                var isReachable = await CrossConnectivity.Current.IsRemoteReachable(ApiWebService.MainIPaddress, 5003, 2000);
                if (isReachable == true)
                {
                    try
                    {
                        constatus = "online";
                        var logintable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUser");
                    }
                    catch (Exception ex)
                    {
                        constatus = "offline";
                    }
                }
                else
                {
                    constatus = "offline";
                }
            }
            else
            {
                constatus = "offline";
            }
        }
        protected override bool OnBackButtonPressed()
        {
            
            App.Current.MainPage = new NavigationPage(new MobileTrackerApp.Views.Login(constatus));
            connectionstatus();
            return true;
        }
    }
}