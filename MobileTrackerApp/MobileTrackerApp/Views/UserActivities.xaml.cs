﻿using MobileTrackerApp.Models;
using MobileTrackerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entry = Microcharts.Entry;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserActivities : ContentPage
    {
        private ObservableCollection<ProjectModel> items = new ObservableCollection<ProjectModel>();
        AssignedActivityViewModel _viewModel;
        string empID;
        public UserActivities(string employeeID)
        {
            InitializeComponent();
            BindingContext = _viewModel = new AssignedActivityViewModel(employeeID, DateTime.Now.ToString());
            _viewModel.empid = employeeID;
            empID = employeeID;
        }

        private void BtnPrevSat_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnPrevSat.Text));
        }

        private void BtnPrevSun_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnPrevSun.Text));
        }

        private void BtnPrevMon_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnPrevMon.Text));
        }

        private void BtnPrevTue_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnPrevTue.Text));
        }

        private void BtnPrevWed_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnPrevWed.Text));
        }

        private void BtnPrevThur_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnPrevThur.Text));
        }

        private void BtnPrevFri_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnPrevFri.Text));
        }

        private void BtnSat_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnSat.Text));
        }

        private void BtnSun_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnSun.Text));
        }

        private void BtnMon_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnMon.Text));
        }

        private void BtnTue_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnTue.Text));
        }
        private void BtnWed_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnWed.Text));
        }
        private void BtnThur_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnThur.Text));
        }
        private void BtnFri_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AssignedActivities(empID, btnFri.Text));
        }
        protected override bool OnBackButtonPressed()
        {
            App.Current.MainPage = new NavigationPage(new Dashboard(empID));
            return true;
        }

    }
}