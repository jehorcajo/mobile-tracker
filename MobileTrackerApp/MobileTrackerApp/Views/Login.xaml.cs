﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using MobileTrackerApp.Models;
using MobileTrackerApp.Services;
using MobileTrackerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using SQLite;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        //ApiServices _service = new ApiServices();
        public UserModel usermodel;
        public LoginViewModel loginviewmodel;
        public SettingModel settingmodel;
        public Login(string constatus)
        {
            InitializeComponent();
            //lowerstack.TranslateTo(0, 800);
            //upperstack.TranslateTo(0, -800);

            loginviewmodel = new LoginViewModel();

            this.BindingContext = loginviewmodel;

            MessagingCenter.Subscribe<LoginViewModel, string>(this, "LoginAlert", (sender, EmpID) =>
            {
                DisplayAlert("", EmpID, "OK");
            });
            //BindingContext = new UserViewModel();
            if (constatus == "online")
            {
                btnSignal.Source = ImageSource.FromFile("online.png");
            }
            else
            {
                btnSignal.Source = ImageSource.FromFile("offline.png");
                DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
            }
            rememberbutton();

            MyImages.Source = ImageSource.FromFile("webtracker.png");
            Task.Delay(100);
            animate();
        }

        public async void animate()
        {
            await Task.Delay(200);
            //await MyImages.FadeTo(0, 500);
            //await MyImages.FadeTo(1, 500);

            await Task.WhenAll(upperstack.TranslateTo(0, 0), lowerstack.TranslateTo(0, 0));

            //await upperstack.TranslateTo(0, 0); 
            //await lowerstack.TranslateTo(0, 0);
            //await btnremember.TranslateTo(0, 0);
            //await btnremember.TranslateTo(0, 100, 500, Easing.SpringIn);


            //await MyImages.RotateTo(180);
            //await MyImages.RotateTo(0, 500, Easing.SpringOut);
        }
        public void rememberbutton()
        {
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                {
                    conn.CreateTable<SettingModel>();
                   
                    var leavedesc = conn.Table<SettingModel>().ToList();
                    var rememberpass = conn.ExecuteScalar<string>("SELECT rememberpass FROM SettingModel WHERE settingID = 1");
                    var rememberempid = conn.ExecuteScalar<string>("SELECT rememberempid FROM SettingModel WHERE settingID = 1");
                    var rememberme = conn.ExecuteScalar<bool>("SELECT rememberme FROM SettingModel WHERE settingID = 1");

                    btnremember.Checked = rememberme;

                    if (btnremember.Checked == true)
                    {
                        EmployeeID.Text = rememberempid;
                        EmpPassword.Text = rememberpass;
                    }
                    else
                    {
                        EmployeeID.Text = "";
                        EmpPassword.Text = "";
                    }
                }
            }
            catch
            {

            }
        }
        private void BtnSetting_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new Settings();
        }
        private async void btnSignal_Clicked(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();

            if (CrossConnectivity.Current.IsConnected)
            {
                var isReachable = await CrossConnectivity.Current.IsRemoteReachable(ApiWebService.MainIPaddress, 5003, 2000);
                if (isReachable == true)
                {
                    try
                    {
                        var logintable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUser");
                        btnSignal.Source = ImageSource.FromFile("online.png");
                        await DisplayAlert("", "You are online", "OK");
                    }
                    catch (Exception ex)
                    {
                        btnSignal.Source = ImageSource.FromFile("offline.png");
                        await DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                    }
                }
                else
                {
                    btnSignal.Source = ImageSource.FromFile("offline.png");
                    await DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                }
            }
            else
            {
                btnSignal.Source = ImageSource.FromFile("offline.png");
                await DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
            }
        }

        private async void GetLogin()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetStringAsync("");
            var users = JsonConvert.DeserializeObject<List<UserModel>>(response);
        }
        private void btnshowpass_Clicked(object sender, EventArgs e)
        {
            if (EmpPassword.IsPassword == true)
            {
                btnshowpass.Source = ImageSource.FromFile("eyedisable.png");
                EmpPassword.IsPassword = false;
            }
            else
            {
                btnshowpass.Source = ImageSource.FromFile("eyeenable.png");
                EmpPassword.IsPassword = true;
            }
        }

        private void btnremember_CheckedChanged(object sender, XLabs.EventArgs<bool> e)
        {
            if (btnremember.Checked == true)
            {
                remembertxt.Text = "true";
                remembertxt.IsVisible = false;
            }
            else
            {
                remembertxt.Text = "false"; 
                remembertxt.IsVisible = false;
            }
        }

        //private void btnLogin_Clicked(object sender, EventArgs e)
        //{
        //    UserModel usermodel = new UserModel();
        //    //DisplayAlert("Notification", "Do you want store this Number ?", "Yes", "No");
        //    //DisplayAlert("", EmpID, "OK");
        //    //MessagingCenter.Send(this, "LoginAlert", "Please fill empty textbox.");
        //    var saltpassword = Convert.ToBase64String(
        //            Common.SaltHashPassword(
        //                Encoding.ASCII.GetBytes(usermodel.HashPassword),
        //                Convert.FromBase64String(usermodel.HashPassword)));


        //    App.Current.MainPage = new NavigationPage(new Dashboard());
        //}
    }
}