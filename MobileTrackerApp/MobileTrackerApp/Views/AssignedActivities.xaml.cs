﻿using MobileTrackerApp.Models;
using MobileTrackerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AssignedActivities : ContentPage
    {
        private ObservableCollection<ActivityModel> items = new ObservableCollection<ActivityModel>();
        AssignedActivityViewModel _viewmodel;
        string empID;
        public AssignedActivities(string EmployeeID, string SelectedDate)
        {
            InitializeComponent();
            BindingContext = _viewmodel = new AssignedActivityViewModel(EmployeeID, SelectedDate);

            mytitle.Title = SelectedDate + " Activities";
            _viewmodel.empid = EmployeeID;
            _viewmodel.SelectedDate = Convert.ToDateTime(SelectedDate);
            empID = EmployeeID;
        }

        protected override bool OnBackButtonPressed()
        {
            App.Current.MainPage = new NavigationPage(new UserActivities(empID));
            return true;
        }
    }
}