﻿using MobileTrackerApp.Models;
using MobileTrackerApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditLeave : ContentPage
    {
        string empid;
        UserLeaveViewModel _viewmodel;
        int ActivityID;
        ActivityModel leavepopulate;
        public EditLeave(ActivityModel leavesedit, string employeeid)
        {
            InitializeComponent();
            if (leavesedit != null)
            {
                BindingContext = _viewmodel = new UserLeaveViewModel();
                _viewmodel.empid = employeeid;
                //BindingContext = new LeaveDescViewModel();
                leavepopulate = leavesedit;
                PopulateDetails(leavepopulate);
                _viewmodel.ActID = ActivityID;
                empid = employeeid;
            }
        }

        private void PopulateDetails(ActivityModel populatedlist)
        {
            ActivityID = populatedlist.ActivityID;
            //leavetypeitem.SelectedItem = populatedlist.leaveType;
            texthour.Text = populatedlist.ActivityHours.ToString();
            DOB.Date = Convert.ToDateTime(populatedlist.ActivityDate.ToString());
        }

        protected override bool OnBackButtonPressed()
        {
            App.Current.MainPage = new NavigationPage(new MobileTrackerApp.Views.UserLeaves(empid));
            return true;
        }
    }
}