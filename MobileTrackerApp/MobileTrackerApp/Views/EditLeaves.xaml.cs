﻿using MobileTrackerApp.ViewModels;
using MobileTrackerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditLeaves : ContentPage
    {
        UserLeaveViewModel _viewmodel;
        ActivityModel _activitymodelleavepopulate;
        int ActivityID;
        string empid;

        public EditLeaves(ActivityModel leavesedit, string employeeid)
        {
            InitializeComponent();
            if (leavesedit != null)
            {
                BindingContext = _viewmodel = new UserLeaveViewModel();
                _viewmodel.empid = employeeid;
                //BindingContext = new LeaveDescViewModel();
                _activitymodelleavepopulate = leavesedit;
                PopulateDetails(_activitymodelleavepopulate);
                _viewmodel.ActID = ActivityID;
                empid = employeeid;
            }
        }
        private void PopulateDetails(ActivityModel populatedlist)
        {
            ActivityID = populatedlist.ActivityID;
            //leavetypeitem.SelectedItem = populatedlist.leaveType;
            texthour.Text = populatedlist.ActivityHours.ToString();
            DOB.Date = Convert.ToDateTime(populatedlist.ActivityDate.ToString());
        }
        protected override bool OnBackButtonPressed()
        {
            App.Current.MainPage = new NavigationPage(new MobileTrackerApp.Views.UserLeaves(empid));
            return true;
        }

    }
}