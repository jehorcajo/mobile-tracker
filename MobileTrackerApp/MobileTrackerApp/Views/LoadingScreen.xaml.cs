﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using MobileTrackerApp.Models;
using MobileTrackerApp.Services;
using MobileTrackerApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using SQLite;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoadingScreen : ContentPage
    {
        public LoadingScreen()
        {
            InitializeComponent();
            MyImages.Source = ImageSource.FromFile("webtracker.png");
            MyImages.Opacity = 0;
            progressbar.ProgressTo(0.1, 500, Easing.Linear);
            lblchecker.Text = "Checking Internet Connections...";
            animate();
            StartupConnection();
        }
        public async void animate()
        {
            
            await Task.Delay(1000);
            //await MyImages.RotateTo(180);
            //await MyImages.RotateTo(0, 500, Easing.SpringOut);
            await MyImages.FadeTo(0, 1000);
            await MyImages.FadeTo(1, 500);
            await progressbar.ProgressTo(0.2, 500, Easing.Linear);
        }
        public async void StartupConnection()
        {
            await Task.Delay(3000);
            await progressbar.ProgressTo(0.3, 500, Easing.Linear);
            HttpClient client = new HttpClient();
            string constatus;
            if (CrossConnectivity.Current.IsConnected)
            {
                lblchecker.Text = "Checking Internet Connections...(Success)";
                
                await progressbar.ProgressTo(0.5, 500, Easing.Linear);
                var isReachable = await CrossConnectivity.Current.IsRemoteReachable(ApiWebService.MainIPaddress, 5003, 2000);
                if (isReachable == true)
                {
                    lblchecker.Text = "Checking Webservice Connection...";
                    await progressbar.ProgressTo(0.7, 500, Easing.Linear);
                    try
                    {
                        var logintable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUser");
                        var tableuser = JsonConvert.DeserializeObject<List<UserModel>>(logintable);
                        using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                        {
                            conn.CreateTable<UserModel>();
                            conn.DeleteAll<UserModel>();
                            conn.InsertAll(tableuser);
                        }
                        lblchecker.Text = "Checking Webservice Connection...(Success)";
                        constatus = "online";
                        await progressbar.ProgressTo(1, 500, Easing.Linear);
                        App.Current.MainPage = new NavigationPage(new Login(constatus));
                        
                    }
                    catch (Exception ex)
                    {
                        lblchecker.Text = "Checking Webservice Connection...(Failed)";
                        constatus = "offline";
                        //btnSignal.Source = ImageSource.FromFile("offline.png");
                        //await App.Current.MainPage.DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                        await progressbar.ProgressTo(1, 500, Easing.Linear);
                        App.Current.MainPage = new NavigationPage(new Login(constatus));

                    }
                }
                else
                {
                    lblchecker.Text = "Checking Webservice Connections...(Failed)";
                    constatus = "offline";
                    await progressbar.ProgressTo(1, 500, Easing.Linear);
                    App.Current.MainPage = new NavigationPage(new Login(constatus));
                }
            }
            else
            {
                lblchecker.Text = "Checking Internet Connections...(Failed)";
                constatus = "offline";
                await progressbar.ProgressTo(1, 500, Easing.Linear);
                App.Current.MainPage = new NavigationPage(new Login(constatus));
            }
        }
        //protected override async void OnAppearing()
        //{
        //    base.OnAppearing();
        //    HttpClient client = new HttpClient();
        //    string constatus;
        //    if (CrossConnectivity.Current.IsConnected)
        //    {
                
        //        var isReachable = await CrossConnectivity.Current.IsRemoteReachable(ApiWebService.MainIPaddress, 5003, 5000);
        //        if (isReachable == true)
        //        {
        //            try
        //            {
        //                var logintable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUser");
        //                var tableuser = JsonConvert.DeserializeObject<List<UserModel>>(logintable);
        //                using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
        //                {
        //                    conn.CreateTable<UserModel>();
        //                    conn.DeleteAll<UserModel>();
        //                    conn.InsertAll(tableuser);
        //                }
        //                constatus = "online";
        //                App.Current.MainPage = new NavigationPage(new Login(constatus));
        //            }
        //            catch (Exception ex)
        //            {
        //                constatus = "offline";
        //                //btnSignal.Source = ImageSource.FromFile("offline.png");
        //                //await App.Current.MainPage.DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
        //                App.Current.MainPage = new NavigationPage(new Login(constatus));
        //            }
        //        }
        //        else
        //        {
        //            constatus = "offline";
        //            App.Current.MainPage = new NavigationPage(new Login(constatus));
        //        }
        //    }
        //    else
        //    {
        //        constatus = "offline";
        //        App.Current.MainPage = new NavigationPage(new Login(constatus));
        //    }
        //}
    }
}