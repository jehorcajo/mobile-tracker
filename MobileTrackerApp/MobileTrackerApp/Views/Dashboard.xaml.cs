﻿using MobileTrackerApp.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MobileTrackerApp.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Dashboard : ContentPage
    {
        bool _ConClosed = true;
        string EMPID;
        string constatus;
        public Dashboard(string empid)
        {
            InitializeComponent();
            
            animate();
            StartupConnection();
            EMPID = empid;
        }

        public async void animate()
        {
            await Task.Delay(100);
            await Task.WhenAll(btnuserprofile.TranslateTo(0, 0), btnleaves.TranslateTo(0, 0), btnactivities.TranslateTo(0, 0), btnreports.TranslateTo(0, 0));
        }

        public async void StartupConnection()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                HttpClient client = new HttpClient();
                var isReachable = await CrossConnectivity.Current.IsRemoteReachable(ApiWebService.MainIPaddress, 5003, 1000);

                if (isReachable == true)
                {
                    //Please Wait Syncing Data in Progress
                    try
                    {
                        var billcodetable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetBillCode");
                        var table1 = JsonConvert.DeserializeObject<List<BillCodeModel>>(billcodetable);

                        var projecttable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetProject");
                        var table2 = JsonConvert.DeserializeObject<List<ProjectModel>>(projecttable);

                        var assignprojecttable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetAssignedProject?empid=" + EMPID);
                        var table3 = JsonConvert.DeserializeObject<List<AssignedProject>>(assignprojecttable);

                        var userleave = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUserLeave?empid=" + EMPID);
                        var table4 = JsonConvert.DeserializeObject<List<UserLeaveModel>>(userleave);

                        var leaves = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetLeaves");
                        var table5 = JsonConvert.DeserializeObject<List<LeavesModel>>(leaves);

                        var activity = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetActivity?empid=" + EMPID);
                        var table6 = JsonConvert.DeserializeObject<List<ActivityModel>>(activity);

                        var utilization = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUtilization");
                        var table7 = JsonConvert.DeserializeObject<List<UtilizationModel>>(utilization);

                        var profile = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetEmployeebyEmpID?empid=" + EMPID);
                        var table8 = JsonConvert.DeserializeObject<List<EmployeeModel>>(profile);
                        btnSignal.Source = ImageSource.FromFile("online.png");
                        using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                        {
                            conn.CreateTable<BillCodeModel>();
                            conn.DeleteAll<BillCodeModel>();
                            conn.InsertAll(table1);

                            conn.CreateTable<ProjectModel>();
                            conn.DeleteAll<ProjectModel>();
                            conn.InsertAll(table2);

                            conn.CreateTable<AssignedProject>();
                            conn.DeleteAll<AssignedProject>();
                            conn.InsertAll(table3);

                            conn.CreateTable<UserLeaveModel>();
                            conn.DeleteAll<UserLeaveModel>();
                            conn.InsertAll(table4);

                            conn.CreateTable<LeavesModel>();
                            conn.DeleteAll<LeavesModel>();
                            conn.InsertAll(table5);

                            conn.CreateTable<ActivityModel>();
                            conn.DeleteAll<ActivityModel>();
                            conn.InsertAll(table6);

                            conn.CreateTable<UtilizationModel>();
                            conn.DeleteAll<UtilizationModel>();
                            conn.InsertAll(table7);

                            conn.CreateTable<EmployeeModel>();
                            conn.DeleteAll<EmployeeModel>();
                            conn.InsertAll(table8);

                            conn.Close();

                            //conn.CreateTable<ActivityModel>();
                            //conn.CreateTable<CompanyModel>();
                            //conn.CreateTable<EmployeeModel>();
                            //conn.CreateTable<LeavesModel>();
                            //conn.CreateTable<ProjectModel>();
                            //conn.CreateTable<UserLeaveModel>();
                            //conn.CreateTable<UserModel>();
                            //conn.CreateTable<UtilizationModel>();
                            //conn.CreateTable<ActivityReportModel>();
                        }
                    }
                    catch (Exception ex)
                    {
                        btnSignal.Source = ImageSource.FromFile("offline.png");
                        await DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                        await App.Current.MainPage.DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                    }
                }
                else
                {
                    btnSignal.Source = ImageSource.FromFile("offline.png");
                    await DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                    await App.Current.MainPage.DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                }
            }
            else
            {
                btnSignal.Source = ImageSource.FromFile("offline.png");
                await DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                await App.Current.MainPage.DisplayAlert("", "You are Currently Offline some of your Data might not up-to-date", "OK");
                //You are currently not connected on the internet some of your Data might not up to date.
            }
        }

        protected override bool OnBackButtonPressed()
        {
            if (_ConClosed)
            {
                ShowDialog();
            }
            return _ConClosed;
        }

        private async void ShowDialog()
        {
            var answer = await DisplayAlert("Warning", "Do you want to Logout?", "Yes", "No");
            if (answer)
            {
                _ConClosed = false;

                HttpClient client = new HttpClient();
                string constatus;
                if (CrossConnectivity.Current.IsConnected)
                {
                    var isReachable = await CrossConnectivity.Current.IsRemoteReachable(ApiWebService.MainIPaddress, 5003, 5000);
                    if (isReachable == true)
                    {
                        try
                        {
                            var logintable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUser");
                            constatus = "online";
                            App.Current.MainPage = new Login(constatus);
                        }
                        catch (Exception ex)
                        {
                            constatus = "offline";
                            App.Current.MainPage = new Login(constatus);
                        }
                    }
                    else
                    {
                        constatus = "offline";
                        App.Current.MainPage = new Login(constatus);
                    }
                }
                else
                {
                    constatus = "offline";
                    App.Current.MainPage = new Login(constatus);
                }
            }
        }

        private async void btnuserprofile_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Profile(EMPID));
        }

        private async void btnleaves_Clicked(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            
            if (CrossConnectivity.Current.IsConnected)
            {
                var isReachable = await CrossConnectivity.Current.IsRemoteReachable(ApiWebService.MainIPaddress, 5003, 5000);
                if (isReachable == true)
                {
                    try
                    {
                        var logintable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUser");
                        await Navigation.PushAsync(new UserLeaves(EMPID));
                    }
                    catch (Exception ex)
                    {
                       await DisplayAlert("Cannot Access", "You are Currently Offline", "OK");
                    }
                }
                else
                {
                    await DisplayAlert("Cannot Access", "You are Currently Offline", "OK");
                    //await Navigation.PushAsync(new UserLeaves(EMPID));
                }
            }
            else
            {
                await DisplayAlert("Cannot Access", "You are Currently Offline", "OK");
            }
        }

        private async void btnactivities_Clicked(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();

            if (CrossConnectivity.Current.IsConnected)
            {
                var isReachable = await CrossConnectivity.Current.IsRemoteReachable(ApiWebService.MainIPaddress, 5003, 5000);
                if (isReachable == true)
                {
                    try
                    {
                        var logintable = await client.GetStringAsync(ApiWebService.MainWebServiceUrl + "api/GetUser");
                        await Navigation.PushAsync(new UserActivities(EMPID));
                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Cannot Access", "You are Currently Offline", "OK");
                    }
                }
                else
                {
                    await DisplayAlert("Cannot Access", "You are Currently Offline", "OK");
                    //await Navigation.PushAsync(new UserLeaves(EMPID));
                }
            }
            else
            {
                await DisplayAlert("Cannot Access", "You are Currently Offline", "OK");
            }
        }

        private async void btnreports_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new WeeklyReportCharts(EMPID));
        }

    }
}