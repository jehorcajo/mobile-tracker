﻿using MobileTrackerApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Profile : ContentPage
    {
        string empid;
       
        public Profile(string employeeID)
        {
            InitializeComponent();
            empid = employeeID;
            populateprofile();
        }


        void populateprofile()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            {
                conn.CreateTable<EmployeeModel>();
                var data = conn.Table<EmployeeModel>().ToList();

                //var data = conn.Table<EmployeeModel>();
                var d1 = data.Where(x => x.EmpID == empid).FirstOrDefault();
                if (d1 != null)
                {
                   
                    userid.Text = d1.EmpID.ToString();
                    txtfirst.Text = d1.Firstname.ToString();
                    txtlast.Text = d1.Lastname.ToString();
                    txtmid.Text = d1.Midname.ToString();
                    txtyear.Text = String.Format("{0:M/d/yyyy}", d1.Processyear);
                    txtemail.Text = d1.Email.ToString();
                }
                else
                    MessagingCenter.Send(this, "LoginAlert", "Invalid Credential");
            }
        }
        protected override bool OnBackButtonPressed()
        {
            App.Current.MainPage = new NavigationPage(new Dashboard(empid));
            return true;
        }
    }
}