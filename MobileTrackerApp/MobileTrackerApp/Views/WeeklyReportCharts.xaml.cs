﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using Microcharts.Forms;
using Entry = Microcharts.Entry;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using MobileTrackerApp.Models;
using MobileTrackerApp.ViewModels;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeeklyReportCharts : ContentPage
    {
        private ObservableCollection<UtilizationModel> items = new ObservableCollection<UtilizationModel>();

        WeeklyViewModel _viewModel;
        string empID;
        public WeeklyReportCharts(string employeeID)
        {
            InitializeComponent();
            BindingContext = _viewModel = new WeeklyViewModel(employeeID);
            _viewModel.empid = employeeID;
            empID = employeeID;
        }
    }
}