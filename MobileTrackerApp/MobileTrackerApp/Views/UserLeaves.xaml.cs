﻿using MobileTrackerApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileTrackerApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserLeaves : ContentPage
    {
        private ObservableCollection<UserLeaveModel> items = new ObservableCollection<UserLeaveModel>();
        string empid;
        bool _ConDelete = true;
        public UserLeaves(string EMPID)
        {
            InitializeComponent();
            empid = EMPID;
        }
        protected override void OnAppearing()
        {
            leavelist.ItemsSource = items;
            items.Clear();

            base.OnAppearing();
            using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
            {
                conn.CreateTable<ActivityModel>();
                var emp = empid.ToLower();
                var list3 = conn.Query<LeavesModel>("SELECT * from LeavesModel");
                var list2 = conn.Query<ActivityModel>("SELECT * from ActivityModel");
                var list = conn.Query<ActivityModel>("SELECT * from ActivityModel WHERE EMPID =?", emp);
                //var leavedesc = conn.Table<LeaveListTable>().ToList();

                leavelist.ItemsSource = list;
            }
        
        }
        private void leavelist_ItemTapped_1(object sender, ItemTappedEventArgs e)
        {
            ActivityModel leavesedit = e.Item as ActivityModel;
            if (leavesedit != null)
            {
                App.Current.MainPage = new NavigationPage(new EditLeave(leavesedit, empid));
            }
        }
        private void Btnadd_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new AddLeaves(empid));
        }

        private async void BtnDelete_Clicked(object sender, EventArgs e)
        {
            if (_ConDelete)
            {
                var answer = await DisplayAlert("Warning", "Do you want to Delete?", "Yes", "No");
                if (answer)
                {
                    var item = (MenuItem)sender;
                    var model = (ActivityModel)item.CommandParameter;

                    using (SQLiteConnection conn = new SQLiteConnection(App.FilePath))
                    {
                        conn.CreateTable<ActivityModel>();
                        var rowcount = conn.Delete<ActivityModel>(model.ActivityID);
                    }
                    OnAppearing();
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            App.Current.MainPage = new NavigationPage(new Dashboard(empid));
            return true;
        }

       

       
    }
}