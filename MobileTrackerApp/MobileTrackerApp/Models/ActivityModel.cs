﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class ActivityModel
    {
        [PrimaryKey]
        public int ActivityID { get; set; }
        public string EMPID { get; set; }
        public bool withLeaves { get; set; }
        public string LeaveType { get; set; }
        public string LeaveDesc { get; set; }
        public string BillCode { get; set; }
        public string CompanyCode { get; set; }
        public DateTime ActivityDate { get; set; }
        public string ActivityName { get; set; }
        public string ActivityDesc { get; set; }
        public string ProjectCode { get; set; }
        public int ActivityHours { get; set; }
        public string DateSubmitted { get; set; }
        public int isactive { get; set; }
        public int isdeleted { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? datemodified { get; set; }
        public string createdby { get; set; }
        public string modifiedby { get; set; }

        public string ActivityDescInfo => $"{ActivityDate.ToString("MMM dd, yyyy")} " + " - Consume Hours" + $" {ActivityHours}";
    }
}
