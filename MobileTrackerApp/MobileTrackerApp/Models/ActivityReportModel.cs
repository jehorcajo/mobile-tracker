﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class ActivityReportModel
    {
        public int ActivityReportID { get; set; }
        public string empID { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public int workinghours { get; set; }
        public int availablehours { get; set; }
        public int leavehours { get; set; }
        public int ytdhours { get; set; }

        public string startenddate => $"{startdate}" + " to " + $" {enddate}";
    }
}
