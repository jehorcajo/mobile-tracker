﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    [Table("UserLeaveModel")]
    public class UserLeaveModel
    {
        [PrimaryKey, AutoIncrement]
        public int LeaveID { get; set; }
        public string EmpID { get; set; }
        public string LeaveType { get; set; }
        public string LeaveDesc { get; set; }
        public string BillCode { get; set; }
        public int LeaveHours { get; set; }
        public string LeaveDate { get; set; }
        public int LeaveDescID { get; internal set; }
    }
}
