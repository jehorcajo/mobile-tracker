﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    [Table("LeavesModel")]
    public class LeavesModel
    {
        [PrimaryKey, AutoIncrement]
        public int LeaveDescID { get; set; }
        public string LeaveCode { get; set; }
        public string LeaveType { get; set; }
        public string LeaveDesc { get; set; }
        public string BillCode { get; set; }
    }
}
