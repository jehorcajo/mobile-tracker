﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class CompanyModel
    {
        public int CID { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public int isactive { get; set; }
        public int isdeleted { get; set; }
        public DateTime? datecreated { get; set; }
        public DateTime? datemodified { get; set; }
        public string createdby { get; set; }
        public string modifiedby { get; set; }
    }
}
