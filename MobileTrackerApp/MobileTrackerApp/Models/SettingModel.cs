﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class SettingModel
    {
        [PrimaryKey]
        public int settingID { get; set; }
        public bool rememberme { get; set; }
        public string rememberuser { get; set; }
        public string rememberpass { get; set; }
        public string rememberempid { get; set; }
        public bool rememberdefaultpage { get; set; }
        public string defaultpage { get; set; }
        public string lastlogin { get; set; }
        public string lastupdate { get; set; }
    }
}
