﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class EmployeeModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IDcount { get; set; }
        public string EmpID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Midname { get; set; }
        public DateTime? Processyear { get; set; }
        public string Email { get; set; }
    }
}
