﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class UtilizationModel
    {
        public int ActivityReportID { get; set; }
        public string empID { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public int workinghours { get; set; }
        public int availablehours { get; set; }
        public int leavehours { get; set; }
        public int ytdhours { get; set; }

        public string startenddate => $"{startdate.ToString("MMM dd, yyyy")}" + " to " + $" {enddate.ToString("MMM dd, yyyy")}";
    }
}
