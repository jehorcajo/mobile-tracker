﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class AssignedProject
    {
        [PrimaryKey, AutoIncrement]
        public int AssignedProjectID { get; set; }
        public string empID { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDesc { get; set; }
        public string CompanyCode { get; set; }
     
    }
}
