﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    [Table("ProjectModel")]
    public class ProjectModel
    {
        [PrimaryKey, AutoIncrement]
        public int ProjectID { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDesc { get; set; }
        public string CompanyCode { get; set; }
 
    }
}
