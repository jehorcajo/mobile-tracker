﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class BillCodeModel
    {
        [PrimaryKey, AutoIncrement]
        public int BillID { get; set; }
        public string BillDesc { get; set; }
        public bool IsProductive { get; set; }
        public bool IsBillable { get; set; }
        public string BillCode { get; set; }
    }
}
