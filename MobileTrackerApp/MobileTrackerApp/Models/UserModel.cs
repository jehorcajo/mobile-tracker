﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTrackerApp.Models
{
    public class UserModel
    {
        public int UserID { get; set; }
        public string EmpID { get; set; }
        public string SaltPassword { get; set; }
        public string HashPassword { get; set; }
        public string LoginAttempt { get; set; }
        public bool isActive { get; set; }
        public bool isDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
